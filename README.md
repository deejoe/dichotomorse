
Tables commonly depict the correspondence between the letters of the English
alphabet and the dots-and-dashes combinations for Morse Code.

Many times, these are arranged in alphabetical order. 

Alas, that arrangement obscures the logic of the assignments and in turn
makes it less obvious how one might go about learning them. 

In short, the assignments are made so that the simplest combinations
represent the most frequently-used letters. 

The English letter E is the most commonly used, so it gets represented by
the simplest symbol: A single dot.

Similarly, T is the most common consonant, and the second-most common letter
overall, so it gets the next simplest symbol: A single dash.

And so it goes. 

The so-called **dichotomous** key shows these correspondences by building up
from a single dot or dash by adding either a dot or dash to the end as one
moves through the table. 

In the way I've drawn them here, moving one column to the right means adding
one more dot or dash. If one moves to the right and up, one adds a dot. If
one moves to the right and down, one adds a dash.

This repository contains four files. The most complete one contains the
entirety of the English alphabet as well as the Morse Code for each letter.
The simplest file is an empty grid with the same layout. Then there is one
file containing letters, but no code. The remaining file contains code, but
no letters. 

The incomplete files are meant to be used as practice pads: One
can try filling them out, either by memorizing progressively where letters
go in the grid (perhaps using various mnemonics) or by following the logic
of the dichotomous key to build up the Morse Code representations.

These files are stored in Flat XML spreadsheet format as saved by
LibreOffice (**.fods**) in the hopes that the files can be put under version
control in a way that makes any changes at least somewhat human-readable. 



